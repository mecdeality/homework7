-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Май 21 2020 г., 12:32
-- Версия сервера: 8.0.18
-- Версия PHP: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `hw7`
--

-- --------------------------------------------------------

--
-- Структура таблицы `finished_projects`
--

CREATE TABLE `finished_projects` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `team` varchar(128) NOT NULL,
  `cost` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `finished_projects`
--

INSERT INTO `finished_projects` (`id`, `name`, `team`, `cost`) VALUES
(3, 'News.com', 'Asala', 18000);

-- --------------------------------------------------------

--
-- Структура таблицы `teamasala`
--

CREATE TABLE `teamasala` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `surname` varchar(128) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `position` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `teamasala`
--

INSERT INTO `teamasala` (`id`, `name`, `surname`, `age`, `position`) VALUES
(1, 'Asylzhan', 'Utegen', 18, 'Boss'),
(2, 'John', 'Suarez', 26, 'Product Manager'),
(3, 'Matt', 'Davella', 19, 'Product Manager'),
(4, 'Alex', 'Walker', 24, 'Frontend Developer'),
(5, 'Arys', 'Ormanov', 18, 'Frontend Developer'),
(6, 'John', 'Fish', 20, 'Backend Developer'),
(7, 'Nathaniel', 'Drew', 21, 'Backend Developer'),
(8, 'Tofik', 'Youtuber', 17, 'Backend Developer'),
(9, 'Barak', 'Obama', 50, 'Backend Developer');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `finished_projects`
--
ALTER TABLE `finished_projects`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `teamasala`
--
ALTER TABLE `teamasala`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `finished_projects`
--
ALTER TABLE `finished_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
