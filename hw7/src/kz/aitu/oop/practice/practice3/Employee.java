package kz.aitu.oop.practice.practice3;

public abstract class Employee {
    private static int id = 0;
    private int EmpId;
    private String name;
    private String surname;
    private int age;
    private String position;
    protected static int pointOfEmployees = 0;

    public Employee(String name, String surname, int age, String position ){
        id++;
        this.EmpId = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.position = position;
    }

    public int getId() {
        return EmpId;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPosition() {
        return position;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void work(){}
}
