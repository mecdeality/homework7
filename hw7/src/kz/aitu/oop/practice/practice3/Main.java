package kz.aitu.oop.practice.practice3;

public class Main {
    public static void main(String[] args) {

        //CREATE ALL EMPLOYESS HERE
        Boss boss = new Boss("Asylzhan", "Utegen", 18);
        ProductManager pm1 = new ProductManager("John", "Suarez", 26);
        ProductManager pm2 = new ProductManager("Matt", "Davella", 19);
        FrontEndDeveloper fd1 = new FrontEndDeveloper("Alex", "Walker", 24);
        FrontEndDeveloper fd2 = new FrontEndDeveloper("Arys", "Ormanov", 18);
        BackEndDeveloper bd1 = new BackEndDeveloper("John", "Fish", 20);
        BackEndDeveloper bd2 = new BackEndDeveloper("Nathaniel", "Drew", 21);
        BackEndDeveloper bd3 = new BackEndDeveloper("Tofik", "Youtuber", 17);
        BackEndDeveloper bd4 = new BackEndDeveloper("Barak", "Obama", 50);

        //WE SHOULD CREATE A NEW TEAM IN ORDER TO START WORK WITH A NEW PROJECT
        Team firstTeam = new Team("Asala");

        firstTeam.addMembers(pm1);
        firstTeam.addMembers(boss);
        firstTeam.addMembers(pm2);
        firstTeam.addMembers(fd1);
        firstTeam.addMembers(fd2);
        firstTeam.addMembers(bd1);
        firstTeam.addMembers(bd2);
        firstTeam.addMembers(bd3);
        firstTeam.addMembers(bd4);

        //HERE WE CREATE OUR PROJECT
        Project project = new Project("News.com");

        //WE MUST GIVE THE PROJECT TO ONE TEAM
        project.setTeam(firstTeam);

        //PRODUCT MANAGERS ARE RESPONSIBLE FOR DEVELOPERS
        pm1.addTeam(fd1);
        pm1.addTeam(fd2);
        pm1.addTeam(bd1);

        pm2.addTeam(bd2);
        pm2.addTeam(bd3);
        pm2.addTeam(bd4);

        //BOSS MANAGES ALL PRODUCT MANAGERS
        boss.addManagers(pm1);
        boss.addManagers(pm2);

        project.start();
        System.out.println(project.costOfAProject());

    }
}
