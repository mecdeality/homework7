package kz.aitu.oop.practice.practice3;

public class FrontEndDeveloper extends Developer  {

    public FrontEndDeveloper(String name, String surname, int age) {
        super(name, surname, age, "Frontend Developer");
    }

    @Override
    public void develop(){
        pointOfEmployees += 40;
    }
    @Override
    public String toString(){
        return super.toString() + ", " + getPosition();
    }
}
