package kz.aitu.oop.practice.practice3;

public class BackEndDeveloper extends Developer {

    public BackEndDeveloper(String name, String surname, int age) {
        super(name, surname, age,"Backend Developer");
    }

    @Override
    public void develop(){
        Employee.pointOfEmployees += 70;
    }
    @Override
    public String toString(){
        return super.toString() + ", " + getPosition();
    }
}
