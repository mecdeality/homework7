// DATABASE CONNECTOR CLASS
package kz.aitu.oop.practice.practice3;

import java.sql.Connection;
import java.sql.DriverManager;

public class Connect_db {
    static Connection con=null;
    public static Connection getConnection()
    {
        if (con != null) return con;
        return getConnection("hw7", "root", "196422");
    }

    private static Connection getConnection(String db_name,String user_name,String password)
    {
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/"+db_name+"?useUnicode=true&serverTimezone=UTC", user_name, password);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return con;
    }
}

