package kz.aitu.oop.practice.practice3;

public abstract class Manager extends Employee {
    public Manager(String name, String surname, int age, String position) {
        super(name, surname, age, position);
    }

    @Override
    public void work(){
        manage();
    }

    public void manage(){}

    public void showTeam(){}

    public String toString(){
        return getName()+" "+getSurname();
    }
}
