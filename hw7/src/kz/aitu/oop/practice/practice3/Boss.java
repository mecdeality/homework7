package kz.aitu.oop.practice.practice3;

import java.util.ArrayList;

public class Boss extends Manager {
    private ArrayList<Manager> managers;
    public Boss(String name, String surname, int age) {
        super(name, surname, age, "Boss");
        managers = new ArrayList<Manager>();
    }
    public void addManagers(Manager m){
        managers.add(m);
    }

    //BOSS COMMANDS TO WORK FOR ALL EMPLOYEES
    public void startWork(){
        for(int i = 0; i < managers.size(); ++i){
            managers.get(i).work();
        }
    }
    public void showAllWorkers(){
        System.out.println(this.toString());
        for(int i = 0; i < managers.size(); ++i){
            managers.get(i).showTeam();
        }
    }

    public int getPointsOfWork(){
        return Employee.pointOfEmployees;
    }

    @Override
    public String toString(){
        return super.toString() + ", " + getPosition();
    }
}
