package kz.aitu.oop.practice.practice3;


import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Team{
    Connection con = Connect_db.getConnection();
    private String name;
    private ArrayList<Employee> employees;
    private Employee teamBoss;

    public Team(String name){
        this.name = name;
        employees = new ArrayList<Employee>();

        //HERE WE CREATE A NEW TABLE IN OUR DB
        String newTeam = "CREATE TABLE " + "team"+ name +" ("
                + "id INT(11),"
                + "name VARCHAR(128),"
                + "surname VARCHAR(128),"
                + "age INT(11), "
                + "position VARCHAR(128), "
                + "PRIMARY KEY(id))";
        try {
            Statement stmt = con.createStatement();
            stmt.executeUpdate(newTeam);
        }
        catch (SQLException e ) {
            System.out.println("An error has occurred on constructor Team()");
            e.printStackTrace();
        }
    }

    public void addMembers(Employee e){
        employees.add(e);
        String eName = e.getName();
        String eSurname = e.getSurname();
        int eAge = e.getAge();
        int eId = e.getId();
        String ePosition = e.getPosition();
        if(ePosition == "Boss") this.teamBoss = e;

        //INSERTING ALL INFORMATION OF EMPLOYEES TO THE TABLE
        try {
            Statement st = (Statement) con.createStatement();
            st.executeUpdate("INSERT INTO " + "team" + name + " VALUES ('"+eId+"','"+eName+"','"+eSurname+"','"+eAge+"','"+ePosition+"')");
        }

        catch (SQLException ex) {
            System.out.println("An error has occurred on addMembers()");
            ex.printStackTrace();
        }

    }
    //TEAM BEGINS TO WORK
    public int startWork(){
        Boss a = (Boss)teamBoss;
        a.startWork();
        return a.getPointsOfWork();
    }

    public String getName() {
        return name;
    }
}
