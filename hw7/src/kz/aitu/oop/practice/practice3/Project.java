package kz.aitu.oop.practice.practice3;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Project {
    Connection con = Connect_db.getConnection();
    private String projectName;
    private int projectPoint; // WE WILL ESTIMATE THE COST BY ITS ProjectPoint
    private Team team;

    public Project(String projectName){
        this.projectName = projectName;
        this.projectPoint = 0;
    }

    public String getProjectName() {
        return projectName;
    }

    public int getProjectPoint(){
        return projectPoint;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public void setTeam(Team t){
        this.team = t;
    }

    public void start(){
        int pnt =  team.startWork();
        this.projectPoint = pnt;

        //AS OUR TABLE NAME FOR PROJECTS IS 'finished_projects', WE INSERT ONLY COMPLETED PROJECTS
        try {
            Statement st = (Statement) con.createStatement();
            st.executeUpdate("INSERT INTO finished_projects(name, team, cost) VALUES ('"+projectName+"','"+team.getName()+"','"+projectPoint*50+"')");
        }

        catch (SQLException ex) {
            System.out.println("An error has occurred on Project.start()");
            ex.printStackTrace();
        }
    }

    //COST FUNCTION
    public String costOfAProject(){
        int cost;
        cost = projectPoint * 50;
        return "Project '"+ projectName +"' costs: "+ cost + "$";
    }
}


