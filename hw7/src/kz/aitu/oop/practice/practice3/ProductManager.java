package kz.aitu.oop.practice.practice3;

import java.util.ArrayList;

public class ProductManager extends Manager {
    private ArrayList<Developer> team;


    public ProductManager(String name, String surname, int age) {
        super(name, surname, age, "Product Manager");
        team = new ArrayList<Developer>();
    }

    public void addTeam(Developer d){
        team.add(d);
    }

    @Override
    public void showTeam(){
        System.out.println(this.toString());
        for(int i = 0; i < team.size(); ++i){
            System.out.println(team.get(i));
        }
    }

    @Override
    public void manage(){
        for(int i = 0; i < team.size(); ++i){
            team.get(i).work();
        }
    }

    @Override
    public String toString(){
        return super.toString() + ", " + getPosition();
    }
}
