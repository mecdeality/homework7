package kz.aitu.oop.practice.practice3;

public abstract class Developer extends Employee {

    public Developer(String name, String surname, int age, String position) {
        super(name, surname, age, position);
    }

    @Override
    public void work(){
        develop();
    }

    public void develop(){}

    public String toString(){
        return getName()+" "+getSurname();
    }
}
